<!DOCTYPE html>
<html>
<head>
	<title>Pos</title>
	 <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800|Open+Sans:300,400,400i,600,600i,700|Roboto:100,300,400,500,700" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/themify-icons.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/select2.min.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row ponto-de-venda">
			<div class="col-md-8 col-sm-12 first-div">
				<div class="item number">
					<form>
						<input type="text" name="s" placeholder="Seleciona o nome do cliente">
						<button><span class="ti-user"></span><span class="ti-plus control-plus"></span></button>
					</form>
				</div>
			
				
				<div class="item item-table-2">
					<div class="pesquisa">
				<?php  for ($b=0; $b <3 ; $b++) { ?>
				<div class="pesq-item" style="padding: 15px 0;">
					<div class="imagem">
						 <img src="img/34578601152056a3a6b7ffa408f3dc9676e6a_Hexen-Trank_quer.jpg"
                         width="50" height="50">
			  	  	</div>
				    <div class="text" style="">	
				    	<span class="pesq-list-prod">
				    		Acer ( <span class="cat-inside">Tecnologia</span> ) - AKZ 200.000,00
				        </span>
				    	<span class="other-title"> Categoria-</span>
				    	<span class="value">Tecnologia - PC</span>
				    	<span class="other-title"> Quantidade - </span>
				    	<span class="value">5</span>
				    	
			  	    </div>
			  	  </div> 
			  	<?php }?>
			  	 
				</div>
					<div class="number2">
					<form>
						<input type="text" name="search2" placeholder="Procure o produto pelo codigo, nome, ou codigo de barras">
					</form>
					
				</div>
					<div class="div-item-chart table-responsive">

					<table class="table-1 table-hover">
						<thead>
							<th>Descricao</th>
							<th>Quantidade</th>
							<th>Subtotal</th>
							<th>Desconto</th>
							<th>Preco</th>
							<th><span class="ti-trash"></span></th>
						</thead>
						<tbody>
						<?php for($a = 0 ; $a < 3; $a++){?>
							<tr>
								<td class="td-desc"><span class="td-span">
								Tomate Pelado</span></td>
								<td><label>Akz</label> 100.000,00 </td>
								<td class="tdqtd">
									<button class="cust-number btn-diminuir">
										<span class="ti-minus"></span>
									</button>
									<input type="number" class="qtd" name="" value="1">
									<button class="cust-number btn-aumentar">
										<span class="ti-plus"></span>
									</button>
								</td>
								<td class="desc-link"><a href="#">0%</a></td>
								<td><label>Akz</label> 1.000.000,00</td>
								<td class="trash"><a href="#"><span class="ti-trash"></span></a></td>
							</tr>
						 <?php } ?>
						 <tr>
								<td class="td-desc"><span class="td-span full-stock">
								Tomate Pelado</span></td>
								<td><label>Akz</label> 100.000,00 </td>
								<td class="tdqtd">
									<button class="cust-number btn-diminuir">
										<span class="ti-minus"></span>
									</button>
									<input type="number" class="qtd" name="" value="1">
									<button class="cust-number btn-aumentar">
										<span class="ti-plus"></span>
									</button>
								</td>
								<td class="desc-link"><a href="#">0%</a></td>
								<td><label>Akz</label> 1.000.000,00</td>
								<td class="trash"><a href="#"><span class="ti-trash"></span></a></td>
							</tr>
							<tr>
								<td class="td-desc"><span class="td-span minimal-stock">
								Tomate Pelado</span></td>
								<td><label>Akz</label> 100.000,00 </td>
								<td class="tdqtd">
									<button class="cust-number btn-diminuir">
										<span class="ti-minus"></span>
									</button>
									<input type="number" class="qtd" name="" value="1">
									<button class="cust-number btn-aumentar">
										<span class="ti-plus"></span>
									</button>
								</td>
								<td class="desc-link"><a href="#">0%</a></td>
								<td><label>Akz</label> 1.000.000,00</td>
								<td class="trash"><a href="#"><span class="ti-trash"></span></a></td>
							</tr>
						</tbody>
					</table>
					</div>
					<div class="table-responsive response">
					<table class="table-2">
						<tbody>
							<tr class="tr-secundary">
								<td>Total de Itens</td>
								<td>10(1 Produtos)</td>
								<td>Total</td>
								<td><label>Akz</label> 100.000,00</td>
							</tr>
							<tr class="tr-secundary">
								<td ><a href="#" class="tool-desc">Desconto %
                                    <span >
                                 	 Judson dos Santos Artur Judson dos
                                 	  Judson dos Santos Artur Judson dos
                                 	   Judson dos Santos Artur Judson dos
                                 	    Judson dos Santos Artur Judson dos
                                 </span>

								</a>
                                 
								</td>
								<td>(10%) 2.500,00</td>
								<td ><a href="#" class="tool-desc">

								Taxa
								<span>
                                 	 Judson dos Santos Artur Judson
                                 </span></a>
									
								</td>
								<td>0,00</td>
							</tr>
							
							<tr rowspan="2" class="tr-total">
								<td>Total a Pagar</td>
								<td></td>
								<td></td>
								<td><label>Akz</label> 100.000,00</td>
							</tr>
						</tbody>
					</table>
					</div>
					<div class="side-buttons">
						<div class="buttons close-and-pause">
							<button class="our-btn" style="background: #F6BB42;"><span class="ti-control-pause"></span> Concluir Depois</button>
							<button class="our-btn" style="background:#DA4453;"><span class="ti-close"></span> Zerar Compra</button>
						</div>
						<div class="buttons payment">
							<button> Pagamento</button>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 second-div">
				<div class="item categories-control">
					<div class="categories-buttons">
					 <button class="our-btn tbbuton" onclick="tb_marcas_categorias(event,'categorias')">
							<span class="ti-layout-grid2"></span>
						Categorias
					</button>
						<button class="our-btn tbbuton" onclick="tb_marcas_categorias(event,'marcas')"
						><span class="ti-widget"></span>Fabricantes/Marcas</button>
					</div>
					<div class="categories-buttons">
						<form class="form-categories">
							<input type="text" name="search2" placeholder="Procure o produto pelo codigo, nome, ou codigo de barras"> <button><span class="ti-search"></span></button>
					    </form>
					</div>
				</div>
				<div class="item show-categories" id="categorias" style="display: block;">
					<?php for ($i=0; $i < 12 ; $i++) { ?>
					<div class="categories-item" >
					<a href="#"  onclick="tb_marcas_categorias(event,'produtos')">
					   <div class="image">
					   	  <img src="img/drink.png" alt="bebidas" title="bebidas" >
					   </div>
					   <div class="desc-category" style="text-align: center;">
					   	   <span class="name">Bebidas</span>
					   	   <span class="ti-layout-grid2" style="display: inline-block; margin: 5px 5px 0 0; 

					   	   "></span>
					   	   <span>234</span>
					   </div>
					 </a>
					  </div>
					<?php } ?>
				</div>
				<div class="item show-categories"  id="marcas">
					<?php for ($i=0; $i < 12 ; $i++) { ?>
					<div class="categories-item">
					<a href="#" onclick="tb_marcas_categorias(event,'produtos')">
					   <div class="image" >
					   	  <img src="img/Coca-Cola.png" alt="bebidas" title="bebidas" >
					   </div>
					   <div class="desc-category" style="text-align: center;">
					   	   <span class="name">Coca Cola</span>
					   	   <span class="ti-layout-grid2" style="display: inline-block; margin: 5px 5px 0 0; 
                             position: relative;
					   	   "></span>
					   	   <span>234</span>
					   </div>
					 </a>
					  </div>
					<?php } ?>
				</div>
				<div class="item show-categories"  id="produtos">
					<?php for ($i=0; $i < 12 ; $i++) { ?>
					<div class="categories-item">
					<a href="#">
					   <div class="image">
					   	  <img src="img/images.jpg" alt="bebidas" title="bebidas" >
					   </div>
					   <div class="desc-category" style="text-align: center;">
					   	   <span class="name">Compal</span>
					   	   <span class="ti-layout-grid2" style="display: inline-block; margin: 5px 5px 0 0; 
                             position: relative;
					   	   "></span>
					   	   <span>234</span>
					   </div>
					 </a>
					  </div>
					<?php } ?>
				</div>
			</div>
		</div>


			  

		</div>
		
	</div>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/select2.min.js"></script>
  <script type="text/javascript" >
  	function tb_marcas_categorias(evt, place) {
    		var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("show-categories");
   			for (i = 0; i < tabcontent.length; i++) {
       			 tabcontent[i].style.display = "none";
    		}

		    tablinks = document.getElementsByClassName("tbbuton");
		    for (i = 0; i < tablinks.length; i++) {
		        tablinks[i].className = tablinks[i].className.replace(" active", "");
		    }
		    document.getElementById(place).style.display = "block";
		    evt.currentTarget.className += " active";
}

  	  $(document).ready( function(){
	  	    var diminuir = document.getElementsByClassName('btn-diminuir');
	  	     var aumentar = document.getElementsByClassName('btn-aumentar');
	  	    for(let i = 0 ; i < diminuir.length; i++){
		  	    diminuir[i].addEventListener('click',function(e){
		  	    	e.preventDefault();
		  	    	this.parentNode.querySelector('.qtd').stepDown();
		  	    })
	  	}
	  	   for(let i = 0 ; i < aumentar.length; i++){
		  	    aumentar[i].addEventListener('click',function(e){
		  	    	e.preventDefault();
		  	    	this.parentNode.querySelector('.qtd').stepUp();
		  	    })
	  	}

	  	
	  }
  	)
  </script>
</body>
</html>